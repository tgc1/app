import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders TODO APP title', () => {
  const { getByText } = render(<App />);
  const title = getByText(/TODO APP/);
  expect(title).toBeInTheDocument();
});
