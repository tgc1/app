import React, {Component} from 'react';
const axios = require('axios');

class TodoList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
    };
  }

  componentDidMount = () => {
    let that = this;
    axios.get('http://localhost:8080/todos/')
    .then(function (response) {
      console.log(response);

      that.setState({
        todos: response.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    console.log(this.state);

    const listItems = this.state.todos.map((todo) =>
      <li key={todo.id}>{todo.title}</li>
    );

    return (
      <div className="TodoList">
        <ul>
          {listItems}
        </ul>

      </div>
    );
  }
}

export default TodoList;
