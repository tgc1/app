import React, {Component} from 'react';
import { Button, Form, Input } from 'reactstrap';
import TodoList from './TodoList';
import './App.css';

const axios = require('axios');

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todo: '',
    };
  }

  onChange = (e) => {
    e.preventDefault();

    this.setState({
      todo: e.target.value,
    });
  }

  onSubmit= (e) => {
    e.preventDefault();

    if (this.state.todo === '') {
      return null;
    }

    axios.post('http://localhost:8080/todos/', {
      title: this.state.todo,
    })
    .then(function (response) {
      console.log(response);
      window.location.reload(false);
    })
    .catch(function (error) {
      console.error(error);
    });
  }

  render() {
    return (
      <div className="App">
        <h1>TODO APP</h1>
        <h2>Ajouter un todo</h2>
          <Form onSubmit={this.onSubmit}>
            <div className="form-group">
              <Input
                id="todo"
                type="text"
                name="todo"
                value={this.state.todo}
                onChange={this.onChange}  
                required
              />
              <Button onClick={this.onSubmit}>Ajouter</Button>
            </div>
          </Form>

        <h2 className="list">Mes Todos</h2>
        <TodoList />
      </div>
    );
  }
}

export default App;
